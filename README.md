## To run the application
```
gradle run
```

## The example input/output directories and the config file are under
```
src/test/resources
```