package ivan.logfilter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class OutputFiles implements AutoCloseable {

    private final File dir;
    private Map<String, PrintWriter> files = new HashMap<>();
    
    public OutputFiles(String dir) {
        this.dir = new File(dir);
    }

    @Override
    public void close() throws Exception {
        files.values().stream().forEach(pw -> pw.close());
    }

    public void write(String name, String line) throws FileNotFoundException {
        PrintWriter pw = null;
        synchronized (files) {
            pw = files.get(name);
            if (null == pw) {
                pw = new PrintWriter(new FileOutputStream(new File(dir, name)));
                files.put(name, pw);
            }
        }

        synchronized (pw) {
            pw.println(line)  ;
        }
    }
}
