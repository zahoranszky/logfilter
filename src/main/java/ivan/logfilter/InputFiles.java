package ivan.logfilter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class InputFiles implements AutoCloseable {

    private final String dir;

    public InputFiles(String dir) {
        this.dir = dir;
    }

    @Override
    public void close() throws Exception {
    }

    public Stream<Path> files() throws IOException {
        return Files.list(Paths.get(dir));
    }
}
