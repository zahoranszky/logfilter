package ivan.logfilter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class ConfigPattern {

    private String name;
    private Pattern pattern;

    public ConfigPattern(String line) {
        try {
            Pattern p = Pattern.compile("(.+):\\s*(.+)");
            Matcher m = p.matcher(line);
            m.find();
            name = m.group(1);
            pattern = Pattern.compile(m.group(2));
        } catch (IllegalStateException | PatternSyntaxException e) {
            throw new PatternException(line);
        }
    }

    public String getName() {
        return name;
    }

    public String getPattern() {
        return pattern.toString();
    }

    public boolean match(String line) {
        return pattern.matcher(line).find();
    }
}
