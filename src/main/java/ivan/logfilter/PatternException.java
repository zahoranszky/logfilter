package ivan.logfilter;

public class PatternException extends RuntimeException {

    public PatternException(String line) {
        super("Wrong pattern line: " + line);
    }
}
