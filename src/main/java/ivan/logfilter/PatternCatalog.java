package ivan.logfilter;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class PatternCatalog {

    private final LineStreamSource lineStreamSource;
    private final List<ConfigPattern> patterns;

    public PatternCatalog(LineStreamSource lineStreamSource) throws IOException {
        this.lineStreamSource = lineStreamSource;
        patterns = lineStreamSource.getStream()
                .map(l -> new ConfigPattern(l)).collect(Collectors.toList());
    }

    public List<ConfigPattern> get() {
        return patterns;
    }
}
