package ivan.logfilter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class LineStreamSource {

    private final String filePath;

    public LineStreamSource(String filePath) {
        this.filePath = filePath;
    }

    public Stream<String> getStream() throws IOException {
        return Files.lines(Paths.get(filePath));
    }
}
