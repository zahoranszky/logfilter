package ivan.logfilter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class Main {

    private PatternCatalog patternCatalog;
    private String inDir;
    private String outDir;

    public Main(String inDir, String outDir, String patternFile) throws IOException {
        patternCatalog = new PatternCatalog(new LineStreamSource(patternFile));
        this.inDir = inDir;
        this.outDir = outDir;
    }

    public static void main(String[] args) throws IOException {
        usageIfNeeded(args);

        try {
            checkArguments(args);
            runIt(args);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }
    }

    private static void runIt(String[] args) throws Exception {
        new Main(args[0], args[1], args[2]).process();
    }

    private static void usageIfNeeded(String[] args) {
        if (args.length != 3) {
            printUsage();
            System.exit(1);
        }
    }

    private static void checkArguments(String[] args) {
        if (!new File(args[0]).exists()) {
            throw new RuntimeException("Input dir does not exist (" + args[0] +")");
        }

        if (!new File(args[1]).exists()) {
            throw new RuntimeException("Output dir does not exist (" + args[1] +")");
        }

        if (!new File(args[2]).exists()) {
            throw new RuntimeException("Pattern file does not exist (" + args[2] +")");
        }
    }

    private static void printUsage() {
        System.out.println("Usage: java ivan.logfilter.Main <input dir> <output dir> <pattern file>");
        System.out.println("Example: java ivan.logfilter.Main ./indir ./outdir ./patterns.txt");
    }

    private void process() throws Exception {
        try (
            InputFiles inputFiles = new InputFiles(inDir);
            OutputFiles outputFiles = new OutputFiles(outDir)
        ) {
            inputFiles.files().parallel().forEach(
                    file -> {
                        try {
                            processFile(file, outputFiles);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
            );
        }
    }

    private void processFile(Path file,  OutputFiles outputFiles) throws IOException {
        Files.lines(file).forEach(line -> {
            processLine(line, outputFiles);
        });
    }

    private void processLine(String line, OutputFiles outputFiles) {
        patternCatalog.get().stream().forEach(pattern -> {
            if (pattern.match(line)) {
                try {
                    outputFiles.write(pattern.getName(), line);
                } catch (FileNotFoundException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }
}
