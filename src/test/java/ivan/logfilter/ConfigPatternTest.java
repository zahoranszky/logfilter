package ivan.logfilter;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ConfigPatternTest {

    @Test
    public void testNameParsing() {
        ConfigPattern configPattern = new ConfigPattern("notfound: .*HTTP\\s404.*");

        assertEquals("notfound", configPattern.getName());
    }

    @Test
    public void testPatternParsing() {
        ConfigPattern configPattern = new ConfigPattern("notfound: .*HTTP\\s404.*");

        assertEquals(".*HTTP\\s404.*", configPattern.getPattern());
    }

    @Test(expected = PatternException.class)
    public void testWrongLine1() {
        ConfigPattern configPattern = new ConfigPattern("blabla");
    }

    @Test(expected = PatternException.class)
    public void testWrongLine2() {
        ConfigPattern configPattern = new ConfigPattern("blabla:");
    }

    @Test(expected = PatternException.class)
    public void testWrongLine3() {
        ConfigPattern configPattern = new ConfigPattern("blabla: [,-\\s]");
    }

    @Test
    public void testLineMatch() {
        ConfigPattern configPattern = new ConfigPattern("notfound: .*HTTP\\s404.*");

        boolean match = configPattern.match("Something HTTP 404 Something else");

        assertTrue(match);
    }

    @Test
    public void testNoLineMatch() {
        ConfigPattern configPattern = new ConfigPattern("notfound: .*HTTP\\s404.*");

        boolean match = configPattern.match("Something HTTP 500 Something else");

        assertFalse(match);
    }


}
