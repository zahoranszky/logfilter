package ivan.logfilter;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;

@RunWith(MockitoJUnitRunner.class)
public class PatternCatalogTest {

    @Mock
    private LineStreamSource lineStreamSource;

    @Before
    public void before() throws IOException {
        Mockito.when(lineStreamSource.getStream()).thenReturn(
                Arrays.asList(new String[]{
                        "notfound: .*HTTP\\s404.*",
                        "ok: .*HTTP\\s200.*"
                }).stream()
        );
    }

    @Test
    public void testPatternNames() throws IOException {
        PatternCatalog patterns = new PatternCatalog(lineStreamSource);

        Iterator<ConfigPattern> it = patterns.get().iterator();
        Assert.assertEquals("notfound", it.next().getName());
        Assert.assertEquals("ok", it.next().getName());
    }

    @Test
    public void testPatterns() throws IOException {
        PatternCatalog patterns = new PatternCatalog(lineStreamSource);

        Iterator<ConfigPattern> it = patterns.get().iterator();
        Assert.assertEquals(".*HTTP\\s404.*", it.next().getPattern());
        Assert.assertEquals(".*HTTP\\s200.*", it.next().getPattern());
    }
}
